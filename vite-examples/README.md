# Bootstrap components
This repository contains examples from the bootstrap components presentation

# Examples without vite
The popover example is without vite. 
The HTML is in the top directory of this repository.
The js and css directory contain the js and css files that are part of the bootstrap framework.
The js and img directories also contain js files and images used in the web pages.

# Examples with vite
the vite-examples directory contains a vite project with some bootstrap examples.
Run using
```
cd vite-examples
npm i
npm run dev
```
This will open up the start page with popover example, typically on http://localhost:5173
There are several other web pages with examples linked from the start page.
You can find the filenames in `vite.config.js`. For example, the following line
```
    				card: resolve(htmlDir, "card.html"),
```
indicates that you can surf to the file `vite-examples/src/html/card.html` at http://localhost:5173/card.html


