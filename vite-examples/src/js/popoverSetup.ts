import {Popover} from 'bootstrap';

const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')!
for (const popover of popoverTriggerList) {
	new Popover(popover);
}