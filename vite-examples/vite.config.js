import {defineConfig} from "vite";
import {dirname, resolve} from "node:path"
import {fileURLToPath} from "node:url"

const __dirname = dirname(fileURLToPath(import.meta.url))
const htmlDir = resolve(__dirname, "src/html");

export default defineConfig({
	base: "./",
	build: {
		rollupOptions: {
			input: {
				popover: resolve(__dirname, "index.html"),
				card: resolve(htmlDir, "card.html"),
				button: resolve(htmlDir, "buttons.html"),
				dropdown: resolve(htmlDir, "dropdownMenu.html"),
			},
		},
	},
});